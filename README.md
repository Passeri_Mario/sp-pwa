# SP PWA

Implémenter les fonctionnalités suivantes :

    - Web manifest 
    - Utiliser le localstorage
    - Mise en cache des ressource 
    - Disponibilité de la page en mode hors connexion
    - Sauvegarde des données (si mode hors connexion) dans un localStorage()
    - Envoi des informations au serveur depuis le localStorage via une requete ajax  

## https://messenger.mariopasseri.eu/

## Note 18/20

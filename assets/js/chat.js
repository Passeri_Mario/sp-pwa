
    // activation de la sidenav
    $(document).ready(function(){
        $('.sidenav').sidenav();
    });

    // activation des inputs
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

    // ativations de select form
    $(document).ready(function(){
        $('select').formSelect();
    });

    // Detection si LocalStorage fonctionne
    if (typeof(Storage) !== "undefined") {

        console.log('LocaleStorage en activé')

        if(localStorage.getItem("Pseudo")){

            let userName = localStorage.getItem("Pseudo");

            // création dynamique de chatDips sur la balise main
            let chatDisplay = document.getElementById("chatDisp");

            // si il existe un elements de la tableau du chat
            if(!localStorage.getItem("chat")){
                localStorage.setItem("chat", JSON.stringify([]));
            }

            let chatHistory = JSON.parse(localStorage.getItem("chat"));

            let chatSession = JSON.parse(localStorage.getItem("chat"));

            // Vérification de la permission de notification push
            if (chatHistory.length === 0) {
                Notification.requestPermission().then(r => Notification);
            }

            // Gestion d'affichage des notifications push via un cookie
            let pos = document.cookie.indexOf( "push=" );

            if( pos >= 0){
                // Notification push est activé
                if (Notification.permission === "granted") {
                    let n = new Notification("Nouveau Message de " + chatHistory[chatHistory.length-1].name,{
                        body : chatHistory[chatHistory.length-1].text,
                        icon: localStorage.getItem("LogoHomme") || localStorage.getItem("Logofemme") || localStorage.getItem("LogoInconnu"),
                        vibrate: [100, 50, 100]
                    });
                }
                // Suppr du cookies
                document.cookie = 'push=; path=/; expires=Thu, 01 Jan 1970 00:00:00 UTC';
            }

            // Début de code pour afficher les précédents messages du localStorage

            chatSession.forEach(myFunction);

            function myFunction(item, index) {

                let b = index;

                function Affichage_Message(){
                    // si un compte utilisateur existe
                    if(chatSession[b].name === userName){
                        let templateDiv =
                            "<div id='msg"+[b]+"' class='message flex'>" +
                            "<img class='avatar' src='"+ chatSession[b].src +"' alt=''/> " +
                            "<p class='text'>" + "<span class='name'>" + chatSession[b].name + " </span>" +
                            "<span class='datetime'> " + chatSession[b].dateTime + "</span>" +
                            "<br>" +chatSession[b].text + "</p>" +
                            "</div>";

                        // si compte existe sinon il en charge rien
                        if(localStorage.getItem("Pseudo")){
                            chatDisplay.innerHTML += templateDiv;
                            document.location.href="/chat#msg"+[b];
                        }
                    }
                }
                // scrool vers un message plus bas
                document.getElementById('chatDisp').scrollTop = document.getElementById('chatDisp').clientHeight;
                Affichage_Message();
            }

            // envoi du message
            let sendMessage = document.getElementById("sendMessage");
            sendMessage.addEventListener("click", function (){
                if(localStorage.getItem("Pseudo")){

                    // parcours le tableau d'insertion
                    for (let i = 0; i < localStorage.length; i++){
                        localStorage.key(i);
                    }

                    // vérification si aucun élément et renseigné dans le message
                    if (document.getElementById('messageInput').value === ""){
                        alert("s'il vous plaît entrez quelque chose dans le chat ... ");
                        return
                    }

                    // création des jours de la semaines
                    let jours_semaines = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
                    let jours_push = new Date();
                    let result = jours_semaines[jours_push.getDay()];

                    // variable des données pour le message
                    let message={
                        name : userName,
                        text : document.getElementById('messageInput').value,
                        dateTime: new Date().toLocaleTimeString('fr-FR') + " " + result,
                        src : localStorage.getItem("LogoHomme") || localStorage.getItem("Logofemme") || localStorage.getItem("LogoInconnu")
                    };

                    // Envoi dans le tableau des messages chat[]
                    chatHistory.push(message);

                    document.getElementById('messageInput').value = "";

                    // traitement des données pour insertion dans le tableau
                    localStorage.setItem("chat", JSON.stringify(chatHistory));
                    let localData = localStorage.getItem("chat");
                    JSON.parse(localData);

                    // création d'un cookie
                    document.cookie = 'push=NewMessage; path=/';

                    document.location.reload();

                }
                else {
                    document.location.href = "/compte";
                }
            })
        }
        else {
            document.location.href = "/compte";
        }
    }
    else {
        console.log('LocaleStorage n\'est pas activé ERREUR !! ');
    }

    // activation de la sidenav
    $(document).ready(function(){
        $('.sidenav').sidenav();
    });

    if (typeof(Storage) !== "undefined") {
        // Si un compte utilisateur existe
        if(localStorage.getItem("Pseudo")){
            document.getElementById('no_session').style.display='none';
            document.getElementById('session').style.display='block';
        }else {
            document.getElementById('no_session').style.display='block';
            document.getElementById('session').style.display='none';
            document.getElementById("nav").style.display="none";
        }

        // bouton vers création du compte
        let no_session = document.getElementById('no_session');
        no_session.addEventListener("click", function() {
            document.location.href = "/compte";
        });

        // bouton vers chat si compte crée
        let session = document.getElementById('session');
        session.addEventListener("click", function() {
            document.location.href = "/chat";
        });
    }
    else {
        console.log('LocaleStorage n\'est pas activé ERREUR !! ');
    }
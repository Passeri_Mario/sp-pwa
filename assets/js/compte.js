
    // activation de la sidenav
    $(document).ready(function(){
        $('.sidenav').sidenav();
    });

    // activation des inputs
    $(document).ready(function() {
        $('input#input_text, textarea#textarea2').characterCounter();
    });

    // ativations de select form
    $(document).ready(function(){
        $('select').formSelect();
    });

    // Detection si LocalStorage fonctionne
    if (typeof(Storage) !== "undefined") {
        console.log('LocaleStorage en activé')
        // Vérification si compte déjà validé
        if (localStorage.getItem("Pseudo")) {
            document.getElementById("pseudo").readOnly = true;
            document.getElementById("compte").style.display = "none";
            document.getElementById("valide_pseudo").style.display = "none";
            document.getElementById("valide_pseudo").style.display = "none";
        }
        else {
            document.getElementById("nav").style.display="none";
            document.getElementById("redirect_chat").style.display = "none";
            document.getElementById("clearLC").style.display = "none";
        }

        // bouton Validation du compte
        let ButtonSubmit = document.getElementById("ButtonSubmit");
        ButtonSubmit.addEventListener("click", function () {

            // création du pseudo
            let Pseudo = document.getElementById('pseudo').value;
            localStorage.setItem("Pseudo", Pseudo);

            let logohomme = document.getElementById('logohomme').value; // logo homme
            let logofemme = document.getElementById('logofemme').value; // logo femme
            let LogoInconnu = "assets/img/logo_inconnu.png"; // logo inconnu

            // Vérification du logo homme est sélectionné
            if (document.getElementById('logohomme').checked) {
                // création du logo homme
                localStorage.setItem("LogoHomme", logohomme);
            }
            else {
                // création du logo inconnu
                localStorage.setItem("LogoInconnu", LogoInconnu);
            }

            // Vérification du logo femme est sélectionné
            if (document.getElementById('logofemme').checked) {
                // création du logo femme
                localStorage.setItem("Logofemme", logofemme);
            }
            else {
                // création du logo inconnu
                localStorage.setItem("LogoInconnu", LogoInconnu);
            }
            document.location.href = "/chat";
        })

        // Suppression des données
        let clearLC = document.getElementById("clearLC");
        clearLC.addEventListener("click", function () {
            localStorage.clear();
            document.location.href = "/";
        })
    }
    else {
        console.log('LocaleStorage n\'est pas activé ERREUR !! ');
    }
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Voici les routes pour enregistrer des itinéraires Web de l'application.
| Les routes sont chargées par $_SERVER['REQUEST_URI']
|
*/

$request = $_SERVER['REQUEST_URI'];

switch ($request) {

    // 404
    default:
        http_response_code(404);
        require __DIR__ . '/controller/404.php';
        break;

    // Page d'atterrissage
    case '/' :
    case '/index':
        http_response_code(200);
        require __DIR__ . '/controller/index.php';
        break;

    // Page du dashboard
    case '/compte':
        http_response_code(200);
        require __DIR__ . '/controller/compte.php';
        break;

    case '/chat':
        http_response_code(200);
        require __DIR__ . '/controller/chat.php';
        break;

}
